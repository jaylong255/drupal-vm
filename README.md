# Drupal 7 Vagrant Box

This vagrant box was developed to create a local environment for raic.org devops, but could easily be purposed for other Drupal sites. I'm not going to go all out on this one, it's going to come with a few less-than-ideal manual instructions. It's just that I almost never get Drupal gigs and Drupal 7 is all but dead. But RAIC depends on it at the moment. Ideally we will update them to Drupal 8 soon. At that point I might consider automating this whole box to get you going on a simple `vagrant up` command. Until then, you'll just need to manually adjust a few things. Maybe even drop and unzip a file or 2. But I'll at least document everything here.

## What this box includes:

 - LAMP stack.  (RAIC has a huge pile of .htaccess rules, thus making it dependent on apache web server and not environment agnostic. )
 - Composer (at this point we only use composer to install Drush, since it's Drupal 7)
 - Drush 8 ( because Drush 9 cannot install Drupal 7, but Drush 8 can. So I went with the latest version that works.)
 - Drupal 7.59 ( I had a bit of trouble trying to use newer releases of Drupal so I wend with the exact version from the production server. I'm hoping to at least update to 7.6 once we get the staging site stable.

## Requirements
- virtualbox
- vagrant
- sql dumps of the databases
- a zip of the file system
- access to the codebase repo
- patience

## Instructions:

### Up your VM
 1. Clone the repo. `git clone git@bitbucket.org:jaylong255/drupal-vm.git drupalvm`
 2. Change into the new folder `cd drupalvm`
 3. Go ahead and `vagrant up`
 4. Give it about 10 minutes. When it's done, you should have a default drupal site at drupal.test. Don't install it, though.
### Import the Databases
 5. Copy the two database dumps into the drupalvm folder.
 6. Unzip both of them and remove the zips.
 7. `unzip raic_portal_drupal_arch.sql.zip && rm raic_portal_drupal_arch.sql.zip`
 8. `unzip raic_portal_civicrm_arch.sql.zip && rm raic_portal_civicrm_arch.sql.zip`
 9. Make sure they are named `raic_portal_drupal_arch.sql` and `raic_portal_civicrm_arch.sql` respectively.
 10. Remote into the VM `vagrant ssh`
 11. Change into the Drupal directory. `cd /var/www/drupalvm`
 12. If you do `ls` you should be able to see the two sql dumps.
 13. Vagrant should have automated the creation of the database user and the two databases, but we still need to import the structure and data, because we're not version controlling several 5 gig files. We're just not doing it. Ideally you want migrations in code with seeders to generate test data, but this is simply not that system where you get to do such things.
 14. Now do `mysql -u raic_portaldb2 -pdrupal raic_portal_drupal_arch < raic_portal_drupal_arch.sql && rm raic_portal_drupal_arch.sql`
 15. Now do `mysql -u raic_portaldb2 -pdrupal raic_portal_civicrm_arch < raic_portal_civicrm_arch.sql && rm raic_portal_civicrm_arch.sql`
 16.  Exit the VM `exit`
### File System and Codebase
 17. Copy the raic.zip into the drupalvm folder.
 18. Now you can delete that drupalvm/drupal folder and unzip the raic.zip `rm -rf drupalvm/drupal unzip raic.zip && rm raic.zip`
 19. Delete all of the cached files. I'll go back and get a list together, it's several directories.
 20. Make sure the base urls in /sites/default/settings.php and /sites/default/civicrm.settings.php match the file structure of the vm. Should start with `/var/www/drupalvm/raic/`
 21. The rest needs to be handled in the drupal admin, which should be working now.
### Version Control
 23. Change into the drupalvm directory.
 24. Delete this repo. `rm -rf .git`
 25. Clone the repo for the codebase. `git clone git@bitbucket.org:jaylong255/raic.git raic-repo`
 26. Move the codebase repo into the root folder (drupal). `mv raic-repo/.git drupal/.git`
 27. Change into the root folder. cd drupal.
 28. Now release anything that diffs from the repo. `git reset --hard`
 29. And pull any changes. `git pull origin master`
 30. Copy .htaccess.local to .htaccess
### Set Paths and Clear Caches
 31. Log into the drupal admin. It's possible that if your local file structure is exactly the same and your sql dumps came from another local install that you won't have to update the paths. But it's unlikely.
 32. In civicrm, there are a couple of places where the base  path and url need to be set in the cms. Under the admin tab, go to system settings. There's one called directories and one called paths I think. Look over these and make sure they point to a real location in your filesystem. This step should make those extension and module errors go away.
 33. Back in the main drupal admin, go to admin -> system. There should be an option for paths and caches. Don't touch the paths button, you just set those and it will blank them out to defaults. Hit the clear cache button and give it a second.
 34. Now go incognito in your browser or shift+f5. Now refresh.
 35. You should be up and running now.

## Links and resources

 - Box based on drupal vm https://github.com/geerlingguy/drupal-vm
 -
